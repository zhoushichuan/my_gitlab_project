import pytest
import os




if __name__=='__main__':
    #执行测试用例，生成测试数据
    pytest.main(['-s','-v','./test_try_01.py','--alluredir','./allure-data'])
    #生成测试报告
    os.system('allure generate ./allure-data -o ./allure-reports --clean')