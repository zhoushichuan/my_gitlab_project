import os
import unittest
import time
from BeautifulReport import BeautifulReport

def run_tests():
    '''接口自动化执行入口'''
    #找到测试用例存放目录
    test_case_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+'\\test_case'
    #加载测试用例
    decover = unittest.defaultTestLoader.discover(test_case_path,pattern='test_*',top_level_dir=None)
    #生成测试报告名称
    now = time.strftime('%Y-%m-%d %H-%M-%S')
    report_name = 'test_result_'+now+'.html'
    # file = open(report_name,'wb')
    # 测试报告存放路径
    test_report_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '\\reports'
    # 执行测试用例
    BeautifulReport(decover).report(description='资金云测试',filename=report_name,report_dir=test_report_path,theme='theme_candy')
if __name__=='__main__':
    run_tests()