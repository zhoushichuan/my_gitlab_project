import pymysql
import yaml
from my_gitlab_project.Polls.api_test.common.read_config import ci_db_config,sit_db_config, tidb_db_config
import xlrd

class Handle_Mysql():
    def __init__(self,db_env,db):
        self.db_env = db_env
        self.db = db
    def conn_mysql(self):
        """连接数据库"""
        if self.db_env == 'ci':
            host = ci_db_config("host" )
            user = ci_db_config("user")
            password = ci_db_config("password")
            db = ci_db_config(self.db)
            port = ci_db_config('port')
            charset = ci_db_config("charset")
        elif self.db_env == 'sit':
            host = sit_db_config("host")
            user = sit_db_config("user")
            password = sit_db_config("password")
            db = sit_db_config(self.db )
            port = sit_db_config('port', )
            charset = ci_db_config("charset")
        elif self.db_env == 'tidb':
            host = tidb_db_config("host")
            user = tidb_db_config("user")
            password = tidb_db_config("password")
            db = tidb_db_config(self.db )
            port = tidb_db_config('port')
            charset = ci_db_config("charset")
        self.conn = pymysql.connect(host=host, user=user, password=password, port=int(port),db=db, charset=charset)
        #得到一个可以执行SQL语句并且将结果作为字典返回的游标
        self.cur = self.conn.cursor(cursor=pymysql.cursors.DictCursor)

    def execute_sql(self, sql):
        """执行操作数据的sql(增,删,改)"""
        self.conn_mysql()
        try:
            # 执行sql语句
            self.cur.execute(sql)
            # 提交事务
            self.conn.commit()
        except IOError as e:
            #有异常，回滚事务
            self.conn.rollback()
            print('插表失败，请检查！',e)
        #关闭数据库连接
        self.close_mysql()
    def query_sql(self, sql):
        """执行查询数据库sql"""
        self.conn_mysql()
        self.cur.execute(sql)
        # 使用 fetchall() 方法获取所有数据
        res = self.cur.fetchall()
        #关闭数据库连接
        self.close_mysql()
        return res

    def close_mysql(self):
        """关闭数据库连接"""
        # 关闭光标对象
        self.cur.close()
        # 关闭数据库连接
        self.conn.close()


if __name__ == '__main__':
    hdb = Handle_Mysql('ci','dubhepayorder')
    res = hdb.query_sql('select * from t_pay_way tpw limit 1;')
    print(type(res))