import random
import xlrd
import pandas as pd
import openpyxl

from my_gitlab_project.Polls.api_test.common.handle_mysql_db import Handle_Mysql

from my_app.common.time_stamps import current_time

def insert_mysql():
    '''插数据到疑似重复表'''
    hdb = Handle_Mysql('ci' ,'dubhepayorder')
    pay_suspected_id_record = hdb.query_sql("select id from t_pay_suspected tps order by id desc limit 1;")
    pay_suspected_id = pay_suspected_id_record[0].get('id')
    for i in range(0,50000):
        r_trade_id = '111' + str(random.randint(6000000, 9000000))+str(random.randint(1,99))+str(random.randint(1,10))
        pay_suspected_id += 1
        res = hdb.execute_sql("INSERT INTO dubhepayorder.t_pay_suspected (id, lessee_id, pay_trade_id, confirm_type, status, operator, affirmant, expire_date, corp_code, pay_amount_cent, currency, payee_account_name, payee_account_no, payer_cpy_code, create_time, update_time, created_by, modified_by, del_flag, version)"
              "VALUES({0}, 5, {1}, 99, 1, '', '', '2021-09-20', '', 10901, '', '', '', '', '2021-09-04 11:00:01', '2021-09-04 11:00:01', 'system', 'System', 0, 0)".format(str(pay_suspected_id),r_trade_id))
    print(res)

def insert_table_sql(table_name):
    '''生成插表的 sql'''
    hdb = Handle_Mysql('sit', 'pay')
    max_id_record = hdb.query_sql("select id from {table_name}  order by id desc limit 1;".format(table_name=table_name))
    print(max_id_record,type(max_id_record))
    max_id = max_id_record[0].get('id')
    print(max_id)
    # 打开Excel文件读取数据
    data = openpyxl.load_workbook('D:\\Work\\work16_\\付款方式映射模板（IBU海外）.xlsx')
    #获取第一张表的名字
    d = data.sheetnames[0]
    print(d)
    # 根据sheet名字获得sheet
    sheet1 = data.get_sheet_by_name('IBU海外')
    print(sheet1)
    #初始化一个list用于保存获取到的sheet内容
    l = []
    for row in sheet1.rows:
        list1 = []
        for cell in row:
            list1.append(cell.value)
        l.append(list1)
    print(l)
    l_sql = []
    current_timme = current_time()
    with open("D:\\Work\\work16_\\付款方式映射模板（IBU海外）sql.txt", 'w', encoding='utf-8') as f:
        for row in l:
            max_id+=1
            print(row)
            sql = " INSERT INTO {table_name} (id, lessee_id, sys_source, payer_country_code, org_pay_way, pay_way, org_pay_way_show, create_time, update_time, created_by, modified_by)" \
                " VALUES({id},{lessee_id},{sys_source},'{payer_country_code}','{org_pay_way}','{pay_way}','{org_pay_way_show}', '{create_time}', '{update_time}', '80003194', '80003194');".format(table_name=table_name,id=max_id,lessee_id=row[0],sys_source=row[1],payer_country_code=row[2],org_pay_way=row[3],pay_way=row[5],org_pay_way_show=row[4],create_time=current_timme,update_time=current_time)
            print(sql)
            l_sql.append(sql)
            f.write(sql+'\r\n')
    print(l_sql)


if __name__=='__main__':
    insert_table_sql('t_pay_way_mapping')