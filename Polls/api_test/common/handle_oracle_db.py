import pymysql
import yaml

from my_gitlab_project.Polls.api_test.common.read_config import oracle_db_config
import pymysql.cursors
import cx_Oracle as cx


class Handle_Oracle:
    def __init__(self,db='cbesit'):
        self.db = db
    def conn_oracle(self):
        """连接数据库"""
        host = oracle_db_config("host")
        user = oracle_db_config("user")
        password = oracle_db_config("password")
        db = oracle_db_config(self.db)
        port = oracle_db_config('port')
        # charset = oracle_db_config("charset")
        self.conn = cx.connect(user, password, f'{host}:{port}/{db}')
        #得到一个可以执行SQL语句并且将结果作为字典返回的游标
        self.cur = self.conn.cursor()

    def execute_sql(self, sql):
        """执行操作数据的sql(增,删,改)"""
        self.conn_oracle()
        try:
            # 执行sql语句
            self.cur.execute(sql)
            # 提交事务
            self.conn.commit()
        except IOError as e:
            #有异常，回滚事务
            self.conn.rollback()
            print('插表失败，请检查！',e)
        #关闭数据库连接
        self.close_mysql()
    def query_sql(self, sql):
        """执行查询数据库sql"""
        self.conn_oracle()
        self.cur.execute(sql)
        # 使用 fetchall() 方法获取所有数据
        res = self.cur.fetchall()
        #关闭数据库连接
        self.close_mysql()
        return res

    def close_mysql(self):
        """关闭数据库连接"""
        # 关闭光标对象
        self.cur.close()
        # 关闭数据库连接
        self.conn.close()


if __name__ == '__main__':
    h = Handle_Oracle()
    # res = h.execute_sql('Update pay_business set status = \'ACSP\',bank_ret_msg = \'测试支付成功\' where trade_out_no = \'BEIC00008969120000\'')
    res = h.execute_sql('Update pay_business set status = \'RJCT\',bank_ret_msg = \'测试支付失败\' where trade_out_no = \'BEIC00008969120000\'')
    print(type(res))

