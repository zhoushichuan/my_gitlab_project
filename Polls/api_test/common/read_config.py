import configparser
import os


def ci_db_config(k):
    '''读取ci数据库配置'''
    config = configparser.ConfigParser() # 类实例化
    # 定义文件路径
    # path = 'D:\Tech\python_project\config\db_conf.ini'
    path = os.path.dirname(os.getcwd())+'\\config\\db_conf.ini'
    config.read(path,encoding='utf-8')
    value = config.get('mysql_ci',k)
    return value
def sit_db_config(k):
    '''读取sit数据库配置'''
    config = configparser.ConfigParser() # 类实例化
    # 定义文件路径
    # path = 'D:\Tech\python_project\config\db_conf.ini'
    path = os.path.dirname(os.getcwd())+'\\config\\db_conf.ini'
    config.read(path,encoding='utf-8')
    value = config.get('mysql_sit',k)
    return value
def tidb_db_config(k):
    '''读取tidb数据库配置'''
    config = configparser.ConfigParser() # 类实例化
    # 定义文件路径
    # path = 'D:\Tech\python_project\config\db_conf.ini'
    path = os.path.dirname(os.getcwd())+'\\config\\db_conf.ini'
    config.read(path,encoding='utf-8')
    #获取key对应的value
    value = config.get('tidb',k)
    return value

def oracle_db_config(k):
    '''读取tidb数据库配置'''
    config = configparser.ConfigParser() # 类实例化
    # 定义文件路径
    # path = 'D:\Tech\python_project\config\db_conf.ini'
    path = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))+'\\config\\db_conf.ini'
    config.read(path,encoding='utf-8')
    #获取key对应的value
    value = config.get('oracle',k)
    print(value)
    return value

if __name__=='__main__':
    oracle_db_config('host')
