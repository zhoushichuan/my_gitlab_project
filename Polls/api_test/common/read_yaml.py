import yaml
import os

def get_yaml_data(k):
    '''读取yaml文件数据'''
    path = os.path.dirname(os.path.abspath(os.path.dirname(__file__))) + '\\config\\headers_config.yaml'
    file = open(path,encoding='utf-8')
    # 将yaml数据转换为字典
    file_data = yaml.load(file,Loader=yaml.SafeLoader)
    print(file_data,type(file_data))
    v = file_data['headers'][k]
    print(v,type(v))
    file.close()
    return v
def get_redis_config(k1,k2):
    '''读取yaml文件数据'''
    path = os.path.dirname(os.path.abspath(os.path.dirname(__file__))) + '\\config\\redis_config.yaml'
    print('当前目录:',os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
    print('path:',path)
    file = open(path,encoding='utf-8')
    # 将yaml数据转换为字典
    file_data = yaml.load(file,Loader=yaml.SafeLoader)
    print(file_data,type(file_data))
    v = file_data[k1][k2]
    print(v,type(v))
    file.close()
    return v
if __name__=='__main__':
    get_redis_config('sit','host1')

