import random
import os
import pytest
import allure

class Test_01:
    '''测试pytest框架'''
    @pytest.mark.parametrize('a',[1,2,3])
    def test_01(self,a):
        print('a的值',a)
        assert a>=1
    @pytest.mark.parametrize('b,c',[('哈哈','呵呵'),('nice','hello')])
    def test_02(self,b,c):
        print('b的值', b)
        print('c的值', c)
        assert b
        assert c
    @pytest.mark.parametrize('d',range(1,10))
    def test_03(self,d):
        print('d的值', d)
        assert d

if __name__=='__main__':
    #执行测试用例，生成测试数据
    pytest.main(['-s','-v'])



