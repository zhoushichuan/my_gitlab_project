import copy

from django.test import TestCase

# Create your tests here.

#
# class Cat:
#     """定义一个猫类"""
#
#     def __init__(self, new_name, new_age):
#         """在创建完对象之后 会自动调用, 它完成对象的初始化的功能"""
#         # self.name = "汤姆"
#         # self.age = 20
#         self.name = new_name
#         self.age = new_age  # 它是一个对象中的属性,在对象中存储,即只要这个对象还存在,那么这个变量就可以使用
#         # num = 100  # 它是一个局部变量,当这个函数执行完之后,这个变量的空间就没有了,因此其他方法不能使用这个变量
#
#     def __str__(self):
#         """返回一个对象的描述信息"""
#         # print(num)
#         return "名字是:%s , 年龄是:%d" % (self.name, self.age)
#
#     def eat(self):
#         print("%s在吃鱼...." % self.name)
#
#     def drink(self):
#         print("%s在喝可乐..." % self.name)
#
#     def introduce(self):
#         # print("名字是:%s, 年龄是:%d" % (汤姆的名字, 汤姆的年龄))
#         # print("名字是:%s, 年龄是:%d" % (tom.name, tom.age))
#         print("名字是:%s, 年龄是:%d" % (self.name, self.age))
#
#
# # 创建了一个对象
# tom = Cat("汤姆", 30)
# print(tom)
# tom.eat()

# class test1():
# #     def test11(self):
# #         a =13
# #         b =12
# #         return a>b
# # print(test1().test11())
#
# list1 = [i for i in range(1,11) if i%2==0]
# print(list1)
#
# str1 ='ab c'
# tu1 = ('a','b')
# set1 = {'a','b','c'}
# list2 = list(set1)
# list2.sort()
# print(list2)
#
# dic1 = {1:'aa',2:'cc',3:'bb'}
# key1 = dic1.keys()
#
# for key in dic1.values():
#     print(key)
# print(key1)
# a = [1,2,3,4,5]
#
# b= [6,7]
# c=a+b
# # print(c)
# a.extend(b)
# print(b)

#
# a = dict([(1,2),('a',3)])
# b = dict(zip([2,3],['b','cc']))
# print(a)
# print(b)


# a = {x:x**2 for x in [1, 2, 3]}
# print(a)
# for key in a.keys():
#     print(key,a[key])
# for k,v in a.items():
#     print(k,v)

# d = {'a':1, 'c':3, 'b':2}
# dd = {'aa':1, 'cc':3, 'bb':2}
# d.update(dd)
# print(d)
#
# ddd = d.copy()
# print(ddd)
# d1 = copy.deepcopy(d)
# d['a']=2
# print(d)
# print(d1)
# d.pop('a')
# print(d)
#
# d.popitem()
# print(d)


# tu = (1,23,3,1,1)
# tu1 = tuple([1,2])
# c = tu.count(1)
# print(tu)
# # print(tu1)
# # print(tu[1])
#
# s1 = {1,2,3,4}
# l1 = [1.3,6]
# s1.update(l1)
# print(s1)
#
# s1 = 'aabcdefg'
# s2= '111'
# s3 = s1+s2
# print(s1[0])
#
#
# print(s1.count('a'))
# print(s1.find('c'))
#
# l1 =['1','b','d','5']
# s1 = ' '+'-'.join(l1)
# print(s1)
# s1.lstrip()
# print(s1)l
# l1 = '12.11'
# l2 = l1.split('.')
# print(l2)
# print(type(l2))
#
# l3='abc_ddd'
# l4=l3.replace('abc','ccc')
# # print(l4)
# #
# # s = 'abcc'
# # l = [1,25,5,5]
# # tu = (1,2,3,4,3)
# # set1 = set(s)
# # set2 = set(l)
# # print(set1)
# # print(set2)
# # print(set(tu))
#
# import copy
# #不可变序列
# a = 'aa11'
# print(id(a))
# a = a+'bb22'
# print(id(a))
# #可变序列
# b = [1,23,4]
# print(id(b))
# b.append(3)
# print(id(b))
#
#
#
#
# a = {'a':5,'b':15,'c':111}
# b = []
# for k,v in a.items():
#     if v%5==0:
#         b.append(k)
#
# print(b)
#
#
#

import copy

#list的浅拷贝
# l1 = ['hello',[1,2,3]]
# l2 = l1[:]
# l2[0] = 'h'
# l2[1].append(4)
# print('l1',str(l1))
# print('l2',str(l2))
#
#list的深拷贝
l1 = ['hello',[1,2,3]]
l2 = copy.deepcopy(l1)
l2[0] = 'h'
l2[1].append(4)
print('l1',str(l1))
print('l2',str(l2))

import time

now = time.strftime("%Y-%m-%d %H-%M-%S",time.localtime())
print(now)

#冒泡排序
def bubbleSort(arr):
    n = len(arr)
    # 遍历所有数组元素
    for i in range(n):
        for j in range(0, n - i - 1):
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
    print(arr)
import time
#装饰器
def outer(func):
    def inner():
        start =time.time()
        func()
        end =time.time()
        cost_time = end - start
        print('函数耗时:',end-start)
        return cost_time
    return inner
@outer
def test_sum():
    sum = 0
    for i in range(10):
        sum += i
        time.sleep(0.1)
    print('装饰器测试:',sum)
if __name__=='__main__':
    arr = [64, 34, 25, 12, 22, 11, 90]
    bubbleSort(arr)
    print("排序后的数组:",str(arr))



















